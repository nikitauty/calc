import { createSlice } from '@reduxjs/toolkit';
import { changeCSSRootVariables } from '../helpers/changeCSSVars';
import { storage } from '../helpers/storage';
import { Theme } from '../types/enumTheme';
import { IThemeState } from '../types/IThemeState';

const initialState: IThemeState = {
  theme: storage.getItem('theme'),
};

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    toggleTheme(state) {
      state.theme === Theme.DARK
        ? (state.theme = Theme.LIGHT)
        : (state.theme = Theme.DARK);
      
      storage.setItem('theme', state.theme);
      changeCSSRootVariables(state.theme);
    },
  },
});

export const { toggleTheme } = themeSlice.actions;
export default themeSlice.reducer;
