import {configureStore} from '@reduxjs/toolkit';
import themeReduser from './ThemeSlice';

export const store = configureStore({
  reducer: {
    theme: themeReduser,
  },
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
