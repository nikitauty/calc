// import { useState } from 'react';
import classNames from 'classnames';
import styles from './styles/main.module.scss';
import { SwitchTheme } from './components/SwitchTheme';
import { Main } from './components/Main';

export const App = () => {
  return (
    <div className={classNames(styles.container)}>
      <main className={classNames(styles.main)}>
        <SwitchTheme />
        <Main />
      </main>
    </div>
  );
};
