import { Theme } from "./enumTheme";

export interface IThemeState {
    theme: Theme | undefined
}