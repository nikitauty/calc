import { Theme } from '../types/enumTheme';

export function changeCSSRootVariables(theme: Theme) {
  const root = document.querySelector(':root') as HTMLElement;

  const components = ['btn-bg', 'color', 'main-bg', 'secondary-btn-bg', 'svg'];

  components.forEach(component => {
    root.style.setProperty(
      `--${component}-default`,
      `var(--${component}-${theme})`
    );
  });
}
