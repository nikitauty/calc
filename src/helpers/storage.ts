import { Theme } from "../types/enumTheme";

export const storage = {
	setItem: (name: string, item: Theme) => {
		localStorage.setItem(name, JSON.stringify(item));
	},
	getItem: (name: string): Theme | undefined => {
		const item = localStorage.getItem(name);
		if (item) {
			return JSON.parse(item);
		}
	},
};
