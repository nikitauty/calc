import classNames from 'classnames';
import styles from '../styles/main.module.scss';
import { NegativeSvg } from './NegativeSvg';
import { DeleteSvg } from './DeleteSvg';
import { useState } from 'react';
import { isBig, isNull } from '../helpers/help';
import Decimal from 'decimal.js';
import { Answer } from './Answer';

export const Main = () => {
  const [firstNumber, setFirstNumber] = useState(0);
  const [oper, setOper] = useState('');
  const [answer, setAnswer] = useState('0');
  const [history, setHistory] = useState('');

  const handleAddNumber = (n: string) => {
    if (isBig(answer)) return;
    if (answer !== '0' && answer !== 'Infinity') {
      setAnswer(prev => prev + n);
      return;
    }
    setAnswer(n);
  };

  const handleClear = () => {
    setAnswer('0');
    setHistory('');
    setFirstNumber(0);
  };

  const handleSetNegative = () => {
    if (isNull(answer)) return;
    if (answer[0] === '-') {
      setAnswer(prev => prev.slice(1));
      return;
    }
    setAnswer(prev => '-' + prev);
  };

  const handleProcent = () => {
    if (isNull(answer)) return;

    const a = new Decimal(answer);
    setAnswer(() => a.dividedBy(100).toString());
  };

  const handleDot = () => {
    if (!answer) return;
    if (answer.includes('.')) return;
    setAnswer(prev => prev + '.');
  };

  const handleDelete = () => {
    if (isNull(answer)) return;
    if (answer.length === 1 || (answer.length === 2 && answer[0] === '-')) {
      setAnswer('0');
      return;
    }
    setAnswer(prev => prev.slice(0, -1));
  };

  const handleSaveOperation = (op: string) => {
    setFirstNumber(+answer);
    setOper(op);
    setAnswer('');
  };

  const handleGetAnswer = () => {
    //----------------------------------
    const op = oper;

    if (!op || !answer) return;

    setHistory(firstNumber + oper + answer);

    const a = new Decimal(firstNumber);
    const b = new Decimal(answer);

    switch (op) {
      case '+':
        setAnswer(() => a.plus(b).toString());
        break;
      case '-':
        setAnswer(() => a.minus(b).toString());
        break;
      case '×':
        setAnswer(() => a.times(b).toString());
        break;
      case '÷':
        setAnswer(() => a.dividedBy(b).toString());
        break;
      default:
        break;
    }

    if (firstNumber === Infinity) {
      setHistory('');
    }

    setOper('');
    setFirstNumber(0);
  };

  return (
    <div className={classNames(styles.calc)}>
      <section className={classNames(styles.last_case)}>
        {history}
        &nbsp;
      </section>
      <section className={classNames(styles.answer)}>
        <Answer answer={answer} />
        &nbsp;
      </section>
      <section className={classNames(styles.buttons)}>
        <button
          className={classNames(styles.btn, styles.btn__secondary_color)}
          onClick={handleClear}
        >
          C
        </button>
        <button
          className={classNames(
            styles.btn,
            styles.btn__secondary_color,
            styles.btn__with_svg
          )}
          onClick={handleSetNegative}
        >
          <NegativeSvg className={styles.svg__fill_color} />
        </button>
        <button
          className={classNames(styles.btn, styles.btn__secondary_color)}
          onClick={handleProcent}
        >
          %
        </button>
        <button
          className={classNames(styles.btn, styles.btn__primary_color)}
          onClick={() => handleSaveOperation('÷')}
        >
          ÷
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('7')}
        >
          7
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('8')}
        >
          8
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('9')}
        >
          9
        </button>
        <button
          className={classNames(styles.btn, styles.btn__primary_color)}
          onClick={() => handleSaveOperation('×')}
        >
          ×
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('4')}
        >
          4
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('5')}
        >
          5
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('6')}
        >
          6
        </button>
        <button
          className={classNames(styles.btn, styles.btn__primary_color)}
          onClick={() => handleSaveOperation('-')}
        >
          -
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('1')}
        >
          1
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('2')}
        >
          2
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('3')}
        >
          3
        </button>
        <button
          className={classNames(styles.btn, styles.btn__primary_color)}
          onClick={() => handleSaveOperation('+')}
        >
          +
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={handleDot}
        >
          .
        </button>
        <button
          className={classNames(styles.btn)}
          onClick={() => handleAddNumber('0')}
        >
          0
        </button>
        <button
          className={classNames(styles.btn, styles.btn__with_svg)}
          onClick={handleDelete}
        >
          <DeleteSvg className={styles.svg__fill_color} />
        </button>
        <button
          className={classNames(styles.btn, styles.btn__primary_color)}
          onClick={handleGetAnswer}
        >
          =
        </button>
      </section>
    </div>
  );
};
