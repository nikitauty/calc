import { FC } from 'react';
import { Error } from './Error';
import classNames from 'classnames';
import styles from '../styles/main.module.scss';

interface Props {
  answer: string;
}

export const Answer: FC<Props> = ({ answer }) => {
  let cls = styles.answer__small_num;

  if (answer.length > 10) cls = styles.answer__medium_num;
  if (answer.length > 14) cls = styles.answer__large_num;
  if (answer.length > 18) cls = styles.answer__xlarge_num;

  return (
    <span className={classNames(cls)}>
      {+answer === Infinity ? <Error /> : answer}
    </span>
  );
};
