import { FC, useEffect, useState } from 'react';
import { useAppDispath } from '../hooks/redux';
import { toggleTheme } from '../store/ThemeSlice';
import styles from '../styles/switch.module.scss';
import { SunSvg } from './SunSvg';
import { MoonSvg } from './MoonSvg';
import { storage } from '../helpers/storage';
import { changeCSSRootVariables } from '../helpers/changeCSSVars';

export const SwitchTheme: FC = () => {
  const dispatch = useAppDispath();

  const theme = storage.getItem('theme');

  const [checked, setChecked] = useState(theme === 'light' ? true : false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(toggleTheme());
    setChecked(e.target.checked);
  };

  useEffect(() => {
    if (!theme) return;
    changeCSSRootVariables(theme);
  });

  return (
    <label>
      <input
        type="checkbox"
        checked={checked}
        onChange={handleChange}
        className={styles.inp}
      />
      <div className={styles.switch}>{checked ? <SunSvg /> : <MoonSvg />}</div>
    </label>
  );
};
